title: "Download"
---

## State of postmarketOS

Make sure you read [state of postmarketOS](/state/) before installing
postmarketOS.

## Release images

* The latest stable release is **postmarketOS [#latest release#]**.
* Development is done in **postmarketOS edge** (keep and eye on the
  [breakage reports](/edge)).
* Devices in the **main** category offer the best experience.
* The **community** category is for devices that are well maintained but may
  lack some important features. Refer to the
  [devices page](https://wiki.postmarketos.org/wiki/Devices) for details.
* All images below use
  [(close to) mainline](https://wiki.postmarketos.org/wiki/(Close_to)_Mainline)
  kernels.

### Known issues

* v23.06: partition does not get expanded automatically, see
  {{issue|2235|pmaports}} for workarounds
* v23.06 and edge: read-only filesystem when using installer images, see
  {{issue|2260|pmaports}} for workarounds


### How to install
* See the [Installation](https://wiki.postmarketos.org/wiki/Installation) guide.
* Join [#postmarketOS on Matrix/IRC](https://wiki.postmarketos.org/wiki/Matrix_and_IRC) if you have questions!

### After you install

* Default username: `user`
* Default password: `147147`
* Switch to a TTY at any point by holding Volume Down and pressing the Power button 3 times.
* See the UI guides for [Phosh](https://wiki.postmarketos.org/wiki/Phosh),
  [Plasma Mobile](https://wiki.postmarketos.org/wiki/Plasma_Mobile),
  [Sxmo](https://sxmo.org/docs).
* SSH can be [enabled](https://wiki.postmarketos.org/wiki/SSH#Start_the_SSH_Daemon).

[#download table#]
**Don't see your device in the list? Search for it
[on the devices page](https://wiki.postmarketos.org/wiki/Devices) and
consider contributing to help it meet the
[community category requirements](https://wiki.postmarketos.org/wiki/Device_categorization).**
