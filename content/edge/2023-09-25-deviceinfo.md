title: "deviceinfo moved from /etc to /usr/share/deviceinfo"
date: 2023-09-25
---

### Breaking changes

The main deviceinfo file has been moved from `/etc/deviceinfo` to
`/usr/share/deviceinfo/deviceinfo`. If you have any local scripts that for any
reasons read it, we recommend to instead source
`/usr/share/misc/source_deviceinfo`. That file has all the necessary
compatibility logic, and will warn you if something goes wrong.

### User action


#### IMPORTANT for mrtest users

If you have any previous packages installed through `mrtest`, you **must** run
`mrtest zap` to remove those **before** upgrading with the instructions below!
Failure to do this can result in `/etc/deviceinfo` being removed and no logic
in place to use the new location. If this happens, you can either choose to 
keep the mrtest and recover by:

```
sudo mv /etc/postmarketos-mvcfg/backup/devicepkg-utils/etc-deviceinfo /etc/deviceinfo
```

or completely remove it from your system:

```
mrtest zap && apk upgrade -a
```

#### "apk upgrade" error

Due to the situation of the packages, next upgrade will certainly produce
an error. Please run `apk fix`, and the error should be gone. If you ever
modified your `/etc/deviceinfo` file, you can find it under
`/etc/postmarketos-mvcfg/backup/devicepkg-utils`. If you still need local
configuration, please read the new `/etc/deviceinfo` file, and adjust it
accordingly.

### The facts

The `deviceinfo` file, arguably one of the most important parts of postmarketOS
has been moved from `/etc/deviceinfo`, to `/usr/share/deviceinfo/deviceinfo`.
This has been the final consequence of a great effort started at FOSDEM, and
involving lots of public ([pma#1836](https://gitlab.com/postmarketOS/pmaports/-/issues/1836))
and private conversations. The main reason is that there was no distinction
between user's customization of the deviceinfo file, and defaults provided
by the packages. Once users would start to modify the file, it would not get
updated anymore and this could lead to serious problems such as non-booting
devices. Another reason is that this is a required step towards having the
same image used by multiple devices.

Related:
[pma#1836](https://gitlab.com/postmarketOS/pmaports/-/issues/1836)
[pma!4283](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/4283)
[Move deviceinfo to /usr/share milestone](https://gitlab.com/groups/postmarketOS/-/milestones/17)
