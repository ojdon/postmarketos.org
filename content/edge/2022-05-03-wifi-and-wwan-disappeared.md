title: "WiFi and WWAN disappeared"
date: 2022-05-03
---

Due to a recent change in Alpine (networkmanager package refactored, pmOS
packaging had to be adjusted), WiFi and WWAN disappeared leaving devices
without a network connection. This has been fixed, but users who found
themselves upgrading to this broken upgrade might have trouble getting their
connection back. Here are three possible remedies that don't require
reinstalling postmarketOS:

 1. Try to look for an older version of the networkmanager package in
 `/var/cache/apk` and downgrade to that temporarily. Version 1.34.0-r2 or older.

 2. Use USB network sharing to share your PC's Internet connection with your
 phone/tablet (this can be a bit tricky to set up though) and then upgrade like
 normal to the fixed packages. See the
 [USB Internet article](https://wiki.postmarketos.org/wiki/USB_Internet) on our
 wiki.

 3. Download the new packages and install them manually from an Alpine mirror.
 The relevant ones are `networkmanager-wifi` and `networkmanager-wwan`, both in
 the community category. See
 [mirrors.alpinelinux.org](https://mirrors.alpinelinux.org) for a list of
 mirrors.

More information in
[pma#1519](https://gitlab.com/postmarketOS/pmaports/-/issues/1519).
